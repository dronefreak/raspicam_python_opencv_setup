from picamera import PiCamera
import time
import cv2
camera = PiCamera()
camera.resolution = (1920, 1080)
camera.framerate = 15
camera.start_preview()
time.sleep(120)
camera.capture('/home/pi/opencv-3.0.0/samples/data/one.jpg')
camera.stop_preview()
