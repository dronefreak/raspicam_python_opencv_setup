# README #

This README is meant as a guide to setup RASpi camera.

Open up a terminal and type: $sudo raspi-config

Enable raspberryPI camera from the menu.

Once the camera is enables, test it by using the camera.py file.

Install OpenCV on your RPi by following this documentation...

http://www.pyimagesearch.com/2015/02/23/install-opencv-and-python-on-your-raspberry-pi-2-and-b/
 

Once configured test opencv by importing cv2 in a python console

Then try out the test_image.py and test_video.py files to make sure that OpenCV works with the raspi camera module